INSERT INTO roles(name) VALUES ('Admin'), ('User');
INSERT INTO categories(name) VALUES ('Sweet Fruits'),('Sour Fruits'),('Bitter Fruits');
INSERT INTO statuses(name) VALUES ('Pending'), ('Processing'), ('Cancelled by User'), ('Cancelled by Admin'), ('Delivered');
INSERT INTO payments(name) VALUES ('Cash on Delivery'), ('Paypal'), ('Stripe');