@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Cart</h1>

<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Item Name:</th>
						<th>Item Price:</th>
						<th>Item Quantity:</th>
						<th>Item Subtotal</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($items as $item)
					<tr>
						<td>{{$item->name}}</td>
						<td>{{$item->price}}</td>
						<td>{{$item->quantity}}</td>
						<td>{{$item->subtotal}}</td>
						<td>
							<form action="/removeitem/{{$item->id}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger" type="submit">Remove from Cart</button>
							</form>
						</td>
					</tr>
					@endforeach
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total: {{$total}}</td>
						<td>
							<form action="/deleteallitems/" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger">Remove all items</button>
							</form>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection