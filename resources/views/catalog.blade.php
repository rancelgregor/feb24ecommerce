@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Bar Menu</h1>

@if(Session::has("message"))
	<h4>{{Session::get('message')}}</h4>
@endif

<div class="row w-100">
	@foreach($items as $indiv_item)
	<div class="col-lg-4 p-3 my-2">
		<div class="card">
			<img class="card-img-top" src="{{$indiv_item->imgPath}}" alt="Nothing" height="300px">
			<div class="card-body">
				<h2 class="card-title">{{$indiv_item->title}}</h2>
				<p class="card-text">{{$indiv_item->description}}</p>
				<p class="card-text">Price: {{$indiv_item->price}}</p>
				<p class="card-text">Category: {{$indiv_item->category->name}}</p>
			</div>
			<div class="card-footer d-flex">
				<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger" type="submit">DELETE</button>
				</form>
				<a href="/edititem/{{$indiv_item->id}}" class="btn btn-success">Edit</a>
			</div>
			<div class="card-footer">
				<form action="/addtocart/{{$indiv_item->id}}" method="POST">
					@csrf
					<input type="number" name="quantity" class="form-control" value="1">
					<button class="btn btn-primary" type="submit">Add to Cart</button>
				</form>
			</div>
		</div>
	</div>
	@endforeach
</div>

@endsection